/*
   This file is part of PwrGridFreqVisualization.

    PwrGridFreqVisualization is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PwrGridFreqVisualization is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Display.h"
#include <Adafruit_NeoPixel.h>
#define PIN_NEO 2

/**
   NeoPixel variables
*/
char int_mode=0; //0=inital state, 1= 50Mhz mode, 2=100Mnz mode
//unsigned char bright = 10;  // NeoPixel brightness
unsigned short last_freq=0; //remembers the currenty shown frequency on the neo pixel

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number
Adafruit_NeoPixel strip = Adafruit_NeoPixel(24, PIN_NEO, NEO_RGB + NEO_KHZ800);

void initStrip() {
  // Initialize NeoPixel
  strip.begin();
  strip.setBrightness(255); //adjust brightness here
  strip.show();
}

unsigned char b_adjust(unsigned char c, unsigned char b){
  return ((c*100)/b)/100;
}

unsigned long setColor(char led,unsigned char b) {
  if (led < 6) return strip.Color(b_adjust(255 - (led % 6) * 20,b), b_adjust(0 + (led % 6) * 30,b), b_adjust(0,b));
  else if (led < 9) return strip.Color(b_adjust(155 - (led % 3) * 15,b), b_adjust(150, b), b_adjust(0,b));
  else if (led < 12) return strip.Color(b_adjust(110 - (led % 3) * 15,b), b_adjust(150 + (led % 3) * 15,b),b_adjust(0,b));
  else if (led < 15) return strip.Color(b_adjust(80 - (led % 3) * 15,b), b_adjust(195 + (led % 3) * 15,b), b_adjust(0,b));
  else if (led < 18) return strip.Color(b_adjust(35 - (led % 3) * 10,b), b_adjust(240 + (led % 3) * 5,b), b_adjust(0,b));
  else if (led < 24) return strip.Color(b_adjust(0,b), b_adjust(255,b), b_adjust(0,b));
}


void show_pixels(unsigned short freq, unsigned char divisor){
  unsigned char i=0;
  unsigned char nbLed=0;
  unsigned char prev_led=0;
  unsigned bright_adder=80;
  if(freq>=50000){
    nbLed=((freq-50000)*10)/divisor;
  }else{
    nbLed=((50000-freq)*10)/divisor;
  }
  if (nbLed > 23) nbLed = 23;
  //set all LEDs in internal to off first
  for (i = 0; i < 24; i++) {
    strip.setPixelColor(i, 0, 0, 0);
  }
  //strip.setBrightness(bright);
  for (i=0; i <= nbLed; i++) {  // Set LED from 0 to right
    if(i==nbLed){
      bright_adder=4;
    }
    if (freq >= 50000) {
      strip.setPixelColor(i, setColor(i,bright_adder));
    }else{
      strip.setPixelColor((24-i)%24, setColor(i,bright_adder));
    }
  }
  strip.show();
}

void show_freq(unsigned short freq,char mode) {
  unsigned char divisor=0;
  int i=0;
  if(mode==0) return;
  
  //change = true;  // true: execute the main loop to turn on led
  //freq = random(49965, 50034) / 1000.0;
  //Index starts at 1 or 23 accorting to the value of frequency
  if (mode == 1) {
    divisor=80;
  }else if (mode == 2) {
    divisor=40;
  }else if (mode == 3) {
    divisor=20;
  }else if (mode == 4) {
    divisor=10;
  }

  if(int_mode!=mode){
    if (mode == 1) {
      for (i = 0; i < 24; i++) {//white mode = each LED is 8Hz maximum range is +/-184mHz
        strip.setPixelColor(i, 255, 255, 255);
      }
    }else if (mode == 2) {//yellow mode = each LED is 4Hz maximum range is +/-93mHz
      for (i = 0; i < 24; i++) {
        strip.setPixelColor(i, 255, 255, 0);
      }
    }else if (mode == 3) {//purple mode = each LED is 2Hz maximum range is +/-46mHz
      for (i = 0; i < 24; i++) {
        strip.setPixelColor(i, 0, 255, 255);
      }
    }else if (mode == 4) {// blue mode = each LED is 2Hz maximum range is +/-23mHz
      for (i = 0; i < 24; i++) {
        strip.setPixelColor(i, 0, 0, 255);
      }
    }
    strip.setBrightness(32);
    strip.show();
    delay(300);
    strip.setBrightness(255);
    show_pixels(freq,divisor);
  }else if(last_freq!=freq){//slow change to a new value
    unsigned short cnt_freq=last_freq;
    unsigned short delay_t=0;
    if(last_freq>freq){
      delay_t=((last_freq-freq)/(divisor/10));
    }else{
      delay_t=((freq-last_freq)/(divisor/10));
    }
    if(delay_t>0){
      delay_t=500/delay_t;
    }
    while(1){
      show_pixels(cnt_freq,divisor);
      delay(delay_t);
      if(last_freq>freq){
        cnt_freq-=divisor/10;
        if(cnt_freq<freq){
          break;
        }
      }else{
        cnt_freq+=divisor/10;
        if(cnt_freq>freq){
          break;
        }
      }
    }
  }
  last_freq=freq;
  int_mode=mode;
}
